(.
    (def x (- 0 60)) (def y 0)
    (def stat "%health")
    (def align "#right #bottom")
    (def color "%health %health / 100 %health / 50")
    (run focus_style))

(call setalign "#left" "#bottom")
(call setsize icon_width icon_height)
(call setcolor 1 1 1 1)

(func position (.
    (call setcursor (/ "#width" 2) "#height")
    (call movecursor 0 (- 0 (* 2 hotbar_height)))
    (call movecursor (- 0 (* 4 hotbar_width)) (- 0 icon_offset))
    (call movecursor (- 0 (* 0.5 hotbar_width)) 0)))
(run position)

(func draw_empty_heart (call drawpicbyname (str dir "emptyheart.png")))
(func draw_half_heart (call drawpicbyname (str dir "halfheart.png")))
(func draw_half_ab_heart (call drawpicbyname (str dir "halfabheart.png")))
(func draw_heart (call drawpicbyname (str dir "heart.png")))
(func draw_ab_heart (call drawpicbyname (str dir "abheart.png")))
(func move_for_heart (call movecursor icon_width 0))

(func generate_heart (.
    (if (> "%health" a) (run draw_heart)
        (if (> "%health" b) (run draw_half_heart)
            (run draw_empty_heart)))
    (run move_for_heart)
))
(func generate_ab_heart (.
    (if (> "%health" a) (run draw_ab_heart)
        (if (> "%health" b) (run draw_half_ab_heart)
            (run draw_empty_heart)))
    (run move_for_heart)
))

(.
    (. (def a  5) (def b  0) (run generate_heart))
    (. (def a 15) (def b 10) (run generate_heart))
    (. (def a 25) (def b 20) (run generate_heart))
    (. (def a 35) (def b 30) (run generate_heart))
    (. (def a 45) (def b 40) (run generate_heart))
    (. (def a 55) (def b 50) (run generate_heart))
    (. (def a 65) (def b 60) (run generate_heart))
    (. (def a 75) (def b 70) (run generate_heart))
    (. (def a 85) (def b 80) (run generate_heart))
    (. (def a 95) (def b 90) (run generate_heart))
)

(if (> "%health" 100) (.
    (run position)
    (call movecursor 0 (- 0 icon_height))
    (.
        (. (def a 105) (def b 100) (run generate_ab_heart))
        (. (def a 115) (def b 110) (run generate_ab_heart))
        (. (def a 125) (def b 120) (run generate_ab_heart))
        (. (def a 135) (def b 130) (run generate_ab_heart))
        (. (def a 145) (def b 140) (run generate_ab_heart))
        (. (def a 155) (def b 150) (run generate_ab_heart))
        (. (def a 165) (def b 160) (run generate_ab_heart))
        (. (def a 175) (def b 170) (run generate_ab_heart))
        (. (def a 185) (def b 180) (run generate_ab_heart))
        (. (def a 195) (def b 190) (run generate_ab_heart))
    )
))
