(call setalign "#left" "#bottom")
(call setsize (* 9 hotbar_width) xp_height)
(call setcolor xp_color 1)

(call setcursor (/ "#width" 2) "#height")
(call movecursor 0 (- 0 (* 2 hotbar_height)))
(call movecursor (- 0 (* 4 hotbar_width)) 0)
(call movecursor (- 0 (* 0.5 hotbar_width)) 0)

(call drawpicbyname (str dir "emptyboss.png"))

(call setcoloralpha 0.75)
(call movecursor vpixw (- 0 vpixh))
(call setsize (- 0 (- vpixw (* 9 hotbar_width))) (- xp_height vpixh vpixh))

(call drawbar "%match_duration" 100)
