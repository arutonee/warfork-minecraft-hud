(.
    (def x (- 0 60)) (def y 0)
    (def stat "%armor")
    (def align "#right #top")
    (def color "%armor / 150 %armor / 150 %armor")
    (run focus_style))

(call setalign "#left" "#bottom")
(call setsize icon_width icon_height)
(call setcolor 1 1 1 1)

(call setcursor (/ "#width" 2) "#height")
(call movecursor 0 (- 0 (* 2 hotbar_height)))
(call movecursor 0 (- 0 icon_height))
(call movecursor (- 0 (* 4 hotbar_width)) (- 0 icon_offset))
(call movecursor (- 0 (* 0.5 hotbar_width)) 0)

(func draw_empty_armor (call drawpicbyname (str dir "emptyarmor.png")))
(func draw_half_armor (call drawpicbyname (str dir "halfarmor.png")))
(func draw_armor (call drawpicbyname (str dir "armor.png")))
(func move_for_armor (call movecursor icon_width 0))

(func generate_armor (.
    (if (> "%armor" a) (run draw_armor)
        (if (> "%armor" b) (run draw_half_armor)
            (run draw_empty_armor)))
    (run move_for_armor)
))

(if (> "%health" 100) (call movecursor 0 (- 0 icon_height)))

(if (> "%armor" 0) (.
    (. (def a   7.5) (def b   0) (run generate_armor))
    (. (def a  22.5) (def b  15) (run generate_armor))
    (. (def a  37.5) (def b  30) (run generate_armor))
    (. (def a  52.5) (def b  45) (run generate_armor))
    (. (def a  67.5) (def b  60) (run generate_armor))
    (. (def a  82.5) (def b  75) (run generate_armor))
    (. (def a  97.5) (def b  90) (run generate_armor))
    (. (def a 112.5) (def b 105) (run generate_armor))
    (. (def a 127.5) (def b 120) (run generate_armor))
    (. (def a 142.5) (def b 135) (run generate_armor))
))
