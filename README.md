# Credits!
Creator of HUDScript Preprocessor - Me

Creator of HUD - Me

Idea and General Help - [Panda](https://pandaptable.moe/) (pandaptable on Discord)

# Notes
- `Minecraft.zip` will store the latest release for this HUD. You can directly extract it in the user basewf folder.
- Use `cg_showSpeed 2` for the "focus-style" HUD.

# Plans for the future
- Instagib support
- CTF support
- HUD working for smaller # of weapons
- Minimap
- Timer (bossbar?)
