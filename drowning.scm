(call setalign "#right" "#bottom")
(call setsize icon_width icon_height)
(call setcolor 1 1 1 1)

(call setcursor (/ "#width" 2) "#height")
(call movecursor 0 (- 0 (* 2 hotbar_height)))
(call movecursor 0 (- 0 icon_height))
(call movecursor (* 4 hotbar_width) (- 0 icon_offset))
(call movecursor (* 0.5 hotbar_width) 0)

(func draw_bubble (call drawpicbyname (str dir "bubble.png")))
(nil (func draw_bubble_pop (call drawpicbyname (str dir "bubblepop.png"))))
(func draw_blank (call drawpicbyname (str dir "blank.png")))
(func move_for_bubble (call movecursor (- 0 icon_width) 0))

(func generate_bubble (.
    (if (> "%drowning" a) (run draw_bubble)
        (run draw_blank))
    (run move_for_bubble)
))

(if (< "%drowning" 120) (.
    (. (def a   0) (run generate_bubble))
    (. (def a  12) (run generate_bubble))
    (. (def a  24) (run generate_bubble))
    (. (def a  36) (run generate_bubble))
    (. (def a  48) (run generate_bubble))
    (. (def a  60) (run generate_bubble))
    (. (def a  72) (run generate_bubble))
    (. (def a  84) (run generate_bubble))
    (. (def a  96) (run generate_bubble))
    (. (def a 108) (run generate_bubble))
))
