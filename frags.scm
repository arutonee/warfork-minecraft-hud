(call setfontsize (* text_size (/ "%vidheight" "#height")))
(call setfontfamily "Minecraft")
(call setalign "#center" "#bottom")

(func position (.
(call setcursor (/ "#width" 2) "#height")
(call movecursor 0 (- 0 (* 2 hotbar_height)))))

(call setcolor 0 0 0 1)

(run position)
(call movecursor (- 0 vpixw) 0)
(call drawstringnum "%score")

(run position)
(call movecursor vpixw 0)
(call drawstringnum "%score")

(run position)
(call movecursor 0 (- 0 vpixh))
(call drawstringnum "%score")

(run position)
(call movecursor 0 vpixh)
(call drawstringnum "%score")

(run position)
(call setcolor 0.56 0.89 0.31 1)
(call drawstringnum "%score")
