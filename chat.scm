(call setfontfamily "Minecraft")
(call setfontsize (/ "%vidheight" 50))
(call setalign "#left" "#top")
(call setsize (/ "#width" 3) chat_height)
(call setcursor 0 (- "#height" icon_height xp_height chat_height (* 2 hotbar_height)))

(call setcolor 0 0 0 0.5)
(call drawchat 1 1 "$whiteimage")
