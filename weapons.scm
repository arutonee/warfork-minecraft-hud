(call setalign "#left" "#top")
(call setcursor (- 0 (- half_hotbar_width (/ "#width" 2))) (- "#height" (* 2 hotbar_height)))

(nil "Weapon Images")
(call setcustomweaponselect (str dir "outline.png"))
(call setcustomweaponicons "#ITEM_GUNBLADE" 1 (str dir "gunblade.png"))
(call setcustomweaponicons "#ITEM_MACHINEGUN" 1 (str dir "machinegun.png"))
(call setcustomweaponicons "#ITEM_RIOTGUN" 1 (str dir "riotgun.png"))
(call setcustomweaponicons "#ITEM_GRENADE_LAUNCHER" 1 (str dir "grenade.png"))
(call setcustomweaponicons "#ITEM_ROCKET_LAUNCHER" 1 (str dir "rocket.png"))
(call setcustomweaponicons "#ITEM_PLASMAGUN" 1 (str dir "plasma.png"))
(call setcustomweaponicons "#ITEM_LASERGUN" 1 (str dir "laser.png"))
(call setcustomweaponicons "#ITEM_ELECTROBOLT" 1 (str dir "electro.png"))

(call movecursor (* hotbar_width (- 0 4)) 0)
(call setsize (* 9 hotbar_width) hotbar_height)
(call setcolor 1 1 1 1)
(call drawpicbyname (str dir "hotbar.png"))

(call movecursor (* hotbar_width 4) 0)

(if (= "%instagib" 0)
    (if "%weapon_item" (.
        (call drawweaponicons hotbar_width 0 hotbar_width hotbar_height)
        (call movecursor (* 0.75 (- hotbar_height (* 3 item_font_size))) (- 0 (- item_font_size hotbar_height)))
        (call drawweaponstrongammo hotbar_width 0 item_font_size)
        (call movecursor (* 0.75 (- 0 (- hotbar_height (* 3 item_font_size)))) (- item_font_size hotbar_height))
    )
)
