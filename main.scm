(call setscale "#defaultscale")

(def dir "gfx/hud/minecraft/")

(def hotbar_height 45)
(def hotbar_width 33.75)
(def half_hotbar_width 16.875)
(def item_font_size 10)

(def icon_offset 10)
(def icon_height 18)
(def icon_width 13.5)

(def xp_color "0.66 0.84 0.55")
(def xp_height 10)

(def chat_height 160)

(def text_size 24)
(def scoreboard_size 16)

(def vpixw 1.5)
(def vpixh 2)


(func focus_style (if (= "%show_speed" 2) (.
    (call setcursor (/ "#width" 2) (/ "#height" 2))
    (call setalign align)
    (call movecursor x y)
    (call movecursor (- 0 vpixw) (- 0 vpixh))
    (call setfontfamily "Minecraft")
    (call setfontsize (* text_size (/ "%vidheight" "#height")))
    (call setcolor 0 0 0 1)
    (call drawstringnum stat)
    (call movecursor vpixw vpixh)
    (call setcolor color 1)
    (call drawstringnum stat)
)))


(nil "Damage Numbers")
(call drawDamageNumbers)

(nil "Crosshair")
(if "%weapon_item" (.
    (call setalign "#center" "#middle")
    (call setcursor (/ "#width" 2) (/ "#height" 2))
    (call drawcrosshair)))

(nil "Chasing")
(if (!= "%chasing" "#notset") (.
    (call setfontfamily "Minecraft")
    (call setfontsize (/ "%vidheight" 36))
    (call setcolor 1 1 1 1)
    (call setcursor 10 10)
    (call setalign "#left" "#top")
    (call drawplayername "%chasing")))

(nil "Pointing")
(.
    (call setfontfamily "Minecraft")
    (call setfontsize (/ "%vidheight" 50))
    (call setcolor 1 1 1 1)
    (call setalign "#center" "#middle")
    (call setcursor (/ "#width" 2) (+ 32 (/ "#height" 2)))
    (call drawpointing))

(nil "Teammates")
(.
    (call setalign "#center" "#middle")
    (call drawteammates))

(include weapons.scm)
(include health.scm)
(include armor.scm)
(include ammo.scm)
(include drowning.scm)
(include speed.scm)
(include timer.scm)
(include frags.scm)
(include chat.scm)
(include scoreboard.scm)
(include keystate.scm)


(say "Ready!")
